#!/usr/bin/python
import os


def check():
    validated = True
    filetocheck = '/Applications/Managed Software Center.app/Contents/MacOS/Managed Software Center'
    if not os.path.exists(filetocheck):
        validated = False

    return validated


def main():
    if check():
        # Our checks passed, so we can skip the bootstrap process.
        exit(0)
    else:
        # Our checks didn't pass, so we need to run the bootstrap process.
        exit(1)


if __name__ == '__main__':
    main()
